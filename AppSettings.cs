﻿using System.Configuration;

namespace Otus.Homework12.SOLID
{
    class AppSettings
    {
		public static int lowNumber
		{
			get
			{
				return int.Parse(
					ConfigurationManager.AppSettings["Low"]);
			}
		}
		public static int highNumber
		{
			get
			{
				return int.Parse(
					ConfigurationManager.AppSettings["High"]);
			}
		}
		public static int Attempts
		{
			get
			{
				return int.Parse(
					ConfigurationManager.AppSettings["Attempts"]);
			}
		}
	}
}
