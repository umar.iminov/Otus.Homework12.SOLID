﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Homework12.SOLID
{
    internal class GameRunner
    {
		private Game _game;

		public GameRunner(GameOfNumbers game)
		{
			_game = game;
		}

		internal void Run()
		{
			_game?.Play();
		}
	}
}
