﻿using System;

namespace Otus.Homework12.SOLID
{
    class RandomNumberGenerator : IGenerator
    {
		private int _lowNumber = AppSettings.lowNumber;
		private int _highNumber = AppSettings.highNumber;

		public RandomNumberGenerator(int? lowNumber = null, int? highNumber = null)
		{
			_lowNumber = lowNumber ?? _lowNumber;
			_highNumber = highNumber ?? _highNumber;
		}
		public int GenerateNumber()
		{
			return new Random().Next(_lowNumber, _highNumber);
		}
	}
}
