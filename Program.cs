﻿using System;

namespace Otus.Homework12.SOLID
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new GameOfNumbers(new RandomNumberGenerator(), new GameConsole());
            var gameRunner = new GameRunner(game);

            gameRunner.Run();
        }
    }
}
