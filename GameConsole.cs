﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Homework12.SOLID
{
    class GameConsole : IGameInterface

    {
		public int Read()
		{
			return int.Parse(Console.ReadLine());
		}

		public void Write(string message)
		{
			Console.WriteLine(message);
		}
	}
}
