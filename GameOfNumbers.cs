﻿namespace Otus.Homework12.SOLID
{
    internal class GameOfNumbers : Game
	{
		public GameOfNumbers(
			IGenerator numberGenerator,
			IGameInterface gameInterface)
			: base(numberGenerator, gameInterface)
		{
		}

		public override void Play()
		{
			var currentAttempt = 1;
			while (!IsGameOver(currentAttempt))
			{
				ShowMessage($"Попытка {currentAttempt} из {_numberAttempts}");
				CheckAttempt(_gameInterface.Read());

				currentAttempt++;
			}
		}

		private void CheckAttempt(int num)
		{
			if (num == _secretNumber)
			{
				_success = true;
			}
			else
			{
				if (num < _secretNumber)
				{
					ShowMessage("Больше!");
				}

				if (num > _secretNumber)
				{
					ShowMessage("Меньше!");
				}
			}
		}

		internal override bool IsGameOver(int currentAttempt)
		{
			if (!_success && currentAttempt <= _numberAttempts)
			{
				return false;
			}
			else
			{
				ShowEndOfGameMessage();
				return true;
			}
		}
	}
}