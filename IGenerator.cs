﻿

namespace Otus.Homework12.SOLID
{
    interface IGenerator
    {
        public int GenerateNumber();
    }
}
