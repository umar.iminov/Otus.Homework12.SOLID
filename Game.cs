﻿
namespace Otus.Homework12.SOLID
{
	abstract class Game : IGame
	{
		protected int _secretNumber;
		protected int _numberAttempts;
		protected IGameInterface _gameInterface;
		protected bool _success;

		public Game(
			IGenerator numberGenerator,
			IGameInterface gameInterface)
		{
			_secretNumber = numberGenerator.GenerateNumber();
			_numberAttempts = AppSettings.Attempts;
			_gameInterface = gameInterface;
			_success = false;
		}

		internal void ShowEndOfGameMessage()
		{
			if (_success)
			{
				ShowMessage("Победа!");
			}
			else
			{
				ShowMessage($"Число: {_secretNumber}");
			}
		}

		internal void ShowMessage(string message)
		{
			_gameInterface.Write(message);
		}

		internal virtual bool IsGameOver(int currentAttempt)
		{
			return !_success;
		}

		public abstract void Play();

	}
}

