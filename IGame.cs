﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Homework12.SOLID
{
    interface IGame
    {
        public void Play();
    }
}
